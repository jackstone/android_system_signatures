# 使用Keystore
```gradle
keyAlias = 'android_system'
storePassword 'android'
keyPassword = 'android'
```
# 生成Keystore

1. 下载`platform.pk8`和`platform.x509.pem`
    - 在Android源码路径`/build/target/product/security/`下
    - [Android 1~9 源码](http://androidxref.com)
    - [Android 7~12 源码](http://aospxref.com)
2. 生成`platform.priv.pem`文件
    ```
    openssl pkcs8 -in platform.pk8 -inform DER -outform PEM -out platform.priv.pem -nocrypt
    ```

3. 生成pkcs12格式的密钥文件,生成`platform.pk12`文件
    ```
    openssl pkcs12 -export -in platform.x509.pem -inkey platform.priv.pem -out platform.pk12 -name android_system
    ```
    输入两次密码：`android`

4. 生成`.keystore`文件
    ```
    keytool -importkeystore -destkeystore platform.keystore -srckeystore platform.pk12 -srcstoretype PKCS12 -srcstorepass android_box -alias android_system
    ```
    输入两次密码：`android`